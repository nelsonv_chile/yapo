package code;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.Elements;
import pages.Start;
import pages.Wd;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class YapoFlowJava {

    Elements elementsObject = new Elements();
    Start startPage = new Start(driver);
    JavascriptExecutor js = (JavascriptExecutor) driver;

    static WebDriver driver = Wd.setupDriver();
    WebDriverWait wait1 = new WebDriverWait(driver, 15);

    public YapoFlowJava() throws Throwable {}

    @Given("^Casa publicada para venta$")
    public void casa_publicada_para_venta() {
        driver.get(elementsObject.HOME);
        driver.manage().window().maximize();
    }

    @When("^Ingreso los datos$")
    public void ingreso_los_datos() {

        startPage.clickbtnPublicar();

        wait1.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@id='category_group']")));
        Select select1 = new Select(driver.findElement(By.xpath("//select[@id='category_group']")));
        select1.selectByIndex(2);

    }

    @Then("^Datos ingresados correctamente$")
    public void datos_ingresados_correctamente() {

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.close();
        driver.quit();

    }


}
